#pragma once

#include <vector>
#include "utils.hpp"

//class defining a constrained hyperplane
struct H{

    IU::Vector _n;
    IU::Matrix _M;

    //"adapted" basis
    IU::Matrix _V;
    IU::Matrix _invV;

public:
    const IU::Matrix &M(){
        return _M;
    }
    const IU::Matrix &V(){
        return _V;
    }
    const IU::Matrix &invV(){
        return _invV;
    }
    const IU::Vector &n(){
        return _n;
    }

    H(std::vector<float> weights, size_t i, size_t b, size_t c){
        init(weights,i,b,c);
        computeV();
    }

    void init(std::vector<float> weights, size_t i ,size_t b, size_t c){
        size_t N=weights.size();
       IU::Vector temp_n = IU::Vector::Zero(N);
        IU::Matrix temp = IU::Matrix::Zero(N-1,N);

        /*

        _n=(0, .. ,0; w_i-b; .. w_i ... w_i+c, 0 , .. , 0)

        */

        for(size_t k=i-b;k<=i+c;k++)
            temp_n(k)=weights[k];

        temp_n.normalize();
        _n=temp_n;


        ///
        ///
        /// WARNING :: TYPO IN THE ARTICLE !!!
        /// sizeA = i-b and not i-b-1
        /// and
        /// sizeD = N-i-c-1 and not N-i-c
        ///
        ///
        /// !!!!
        ///
        size_t sizeA,sizeB,sizeC,sizeD;
        sizeA=i-b;
        sizeB=b;
        sizeC=c;
        sizeD=N-i-c-1;

        /*
          _M = (A    0    )
               (   B 0    )
               (     0 C  )
               (     0   D)
        */

        IU::Matrix A=IU::Matrix::Zero(sizeA,sizeA) ,
                B=IU::Matrix::Zero(sizeB,sizeB) ,
                C=IU::Matrix::Zero(sizeC,sizeC) ,
                D=IU::Matrix::Zero(sizeD,sizeD);

        /*

          A=(w1  w2 ..  wi-b-1)
            (0   w2 ..     .  )
            (.             .  )
            (0   0  ..  wi-b-1)


         */


        for(size_t r=0;r<sizeA;r++){
            for(size_t c=r;c<sizeA;c++){

                A(r,c)=weights[c];

            }
        }



        /*

          B=(-wi-b      0    ..       0 )
            (-wi-b   -wi-b+1 ..       0 )
            (.                        . )
            (-wi-b   -wi-b+1  ..   -wi-1)


         */

        for(size_t r=0;r<sizeB;r++){
            for(size_t c=0;c<=r;c++){
                B(r,c)=-weights[c+sizeA];
            }
        }

        /*

          C=( wi+1     wi+2    ..       wi+c )
            (  0       wi+2    ..       wi+c )
            (  .                          .  )
            (  0         0     ..       wi+c )

         */


        for(size_t r=0;r<sizeC;r++){
            for(size_t c=r;c<sizeC;c++){
                C(r,c)=weights[sizeA+sizeB+c+1];

            }
        }

        /*

          D=(-wi+c+1      0     ..    0 )
            (-wi+c+1   -wi+c+2  ..    0 )
            (.                        . )
            (-wi+c+1   -wi+c+2  ..   -wN)


         */


        for(size_t r=0;r<sizeD;r++){
            for(size_t c=0;c<=r;c++){
                    D(r,c)=-weights[c+sizeA+sizeB+sizeC+1];
            }
        }

        temp.block(0,0,sizeA,sizeA)=A;
        temp.block(sizeA,sizeA,sizeB,sizeB)=B;
        temp.block(sizeA+sizeB,sizeA+sizeB+1,sizeC,sizeC)=C;
        temp.block(sizeA+sizeB+sizeC,sizeA+sizeB+sizeC+1,sizeD,sizeD)=D;

        _M=temp;
    }

    void computeV(){

        size_t N=_M.cols();
        IU::Matrix tempV=IU::Matrix::Zero(N,N),tempInvV=IU::Matrix::Zero(N,N);


        IU::Matrix system(N,N);
        IU::Vector sol(N);

        tempV.col(0) = _n/_n.norm();

        for(size_t j=1;j<N;j++){

            system.row(0) = _n;

            for(size_t iSystem=1;iSystem<j;iSystem++){

                system.row(iSystem) = _M.row(iSystem-1);
            }

            for(size_t iSystem=j+1;iSystem<N;iSystem++){

                system.row(iSystem-1) = _M.row(iSystem-1);
            }

            //We square the system in order to be able to get its eigenvalues.
            //TODO : Find a better way to do this ?

            system.row(N-1) = system.row(N-2);

//            std::cout<<" j : \n"<<system<<"\n";
            Eigen::EigenSolver<Eigen::MatrixXf> solved(system);

            float minNormEigenValue;
            float norm;

            //we're looking for a solution of System.X=0; however, X=0 is the trivial solution returned by the solver
            //so we want to look at EigenVectors associated to 0 to find a non zero solution

            //we first select the eigenValue (complex) of smallest norm; we will consider it to be 0
            minNormEigenValue=std::abs(solved.eigenvalues()(0));
            size_t indexMinEigenValue=0;
            for(int k=1;k<solved.eigenvalues().rows();k++){
                norm=std::abs(solved.eigenvalues()(k));
                if(norm<minNormEigenValue){
                    indexMinEigenValue=k;
                    minNormEigenValue=norm;
                }
            }

            //we then copy the real value of the associated vector in the vector sol
            //(the complex part of the eigenvector should be 0 anyway)

            for(int p=0;p<sol.rows();p++){
                sol(p) = solved.eigenvectors().col(indexMinEigenValue)(p).real();
            }

            if(sol.dot(_M.row(j-1)) < 0){
                sol *= -1;
            }

            tempV.col(j)=sol/sol.norm();

        }

        //we compute the inverse of tempV
        tempInvV = tempV.fullPivLu().solve(IU::Matrix::Identity(N,N));

        _V=tempV;
        _invV=tempInvV;
//        std::cout<<"Result :  \n"<<_V<<"\n";
    }

    //Algorithm from the supplementary materials of the paper
    IU::Vector computeClosestPoint(const IU::Vector &f){
        IU::Vector fStar;
        size_t N=f.rows();
        std::vector<size_t> I,J; //J is complementary to I

        IU::Vector d = _invV*f;
        I.push_back(0);

        bool cond;

        for(int k=1;k<f.rows();k++){
            cond=(d(k)<0 && (_V.col(k).transpose()*f)<0.0f);
            if(cond)
                I.push_back(k);
            else
                J.push_back(k);
        }

        IU::Matrix V2(N,N);
        for(size_t k=0;k<J.size();k++){
            V2.col(k)=_V.col(J[k]);
        }
        for(size_t k=0;k<I.size();k++){
            V2.col(k+J.size())=_V.col(I[k]);
        }

        IU::Matrix V3=IU::gramschmidtCol(V2);

        IU::Matrix V4=V3.rightCols(I.size());

        IU::Vector c=V4.transpose()*f;

        fStar = f - V4*c;

        return fStar;
    }
};

//class defining the zero set of an operator Oi
struct ZeroSet{
    size_t _i;
    size_t _N;
    std::vector<H> _hs;

public:

    ZeroSet(std::vector<float> weights, size_t i){
        _i=i;
        _N=weights.size();
        computeHs(weights,_i);
    }

    void computeHs(std::vector<float> weights,size_t i){
        size_t N=weights.size();
        for(size_t c=0;c<=N-i-1;c++){
            for(size_t b=0;b<=i;b++){
                _hs.push_back(H(weights,_i,b,c));
            }
        }
    }

    IU::Vector computeClosestPoint(const IU::Vector &f, IU::Vector &n){
        IU::Vector fStar,fTemp;
        float minDistance,distanceTemp;
        size_t indexClosestH;

       fStar=_hs[0].computeClosestPoint(f);
        minDistance=(f-fStar).norm();
        indexClosestH=0;
        for(size_t k=1;k<_hs.size();k++){
         fTemp=_hs[k].computeClosestPoint(f);
            distanceTemp=(f-fTemp).norm();
            if(distanceTemp<minDistance){
                minDistance=distanceTemp;
                fStar=fTemp;
                indexClosestH=k;
            }
        }

        n=_hs[indexClosestH].n();

        return fStar;
    }

};

//class defining an operator over R^N
struct Op{
    ZeroSet _z;

public:

    Op(std::vector<float> weights,size_t i ):
        _z(ZeroSet(weights,i))
    {}

    float eval(const IU::Vector& f){
        IU::Vector n;
        IU::Vector fStar = _z.computeClosestPoint(f,n);

        return IU::sign(f.dot(n))*(f-fStar).norm();
    }

    IU::Vector grad(const IU::Vector& f){
        IU::Vector n;
        IU::Vector fStar = _z.computeClosestPoint(f,n);
        float norm=(f-fStar).norm();
        if(norm>1e-4)
            return IU::sign(f.dot(n)) * (f-fStar)/(f-fStar).norm();
        else
            return IU::sign(f.dot(n)) * (f-fStar);
    }

    //For computation speed purpose, one might consider calling computeClosestPoint only once, and use the result
    //to compute both the gradient and the value of the operator at f :
    void evalAndGrad(const IU::Vector& f, float& val, IU::Vector& grad){
        IU::Vector n;
        IU::Vector fStar = _z.computeClosestPoint(f,n);

        float sign=IU::sign(f.dot(n));
        IU::Vector diff=f-fStar;

        float norm=diff.norm();
        val=sign*norm;

        if(norm>1e-6)
            grad=sign*diff/diff.norm();
        else
            grad=n;
    }

};


struct CorrectedField : public IU::scalarField {
    std::vector<IU::scalarField *> _fs;
    std::vector<float> _offsetThicknesses;
    Op _o;
public:

    CorrectedField(std::vector<IU::scalarField *> fs, std::vector<float> weights, size_t i, std::vector<float> thicknesses):
    _fs(fs),_o(weights,i)
    {
        size_t N = fs.size();
        _offsetThicknesses.resize(N,0);
        std::vector<float> sumThickness(N);

        _offsetThicknesses[i]=0;
        sumThickness[i]=0;
//        if(i>0)
//            sumThickness[i-1]=thicknesses
        for(int k=i-1;k>=0;k--){
            sumThickness[k]=sumThickness[k+1]+ thicknesses[k+1]/2 + thicknesses[k]/2;
            _offsetThicknesses[k]=-sumThickness[k];
        }
        for(size_t k=i+1;k<N;k++){
            sumThickness[k]=sumThickness[k-1]+ thicknesses[k-1]/2 + thicknesses[k]/2;
            _offsetThicknesses[k]=sumThickness[k];
        }

//        std::cout<<"Thicknesses : ";
//        for(size_t i=0;i<N;i++){
//            std::cout<<offsetThicknesses[i]<<" ; ";
//        }
//        std::cout<<"\n";

    }


    float eval(const IU::vec3 &p){
        IU::Vector f(_fs.size());
        for(int i=0;i<f.rows();i++){
            f(i)=_fs[i]->eval(p)+_offsetThicknesses[i];
        }

        return _o.eval(f);
    }

    IU::vec3 grad(const IU::vec3 &p){
        IU::Vector f(_fs.size());
        for(int i=0;i<f.rows();i++){
            f(i)=_fs[i]->eval(p)+_offsetThicknesses[i];
        }
        IU::Vector gradOp=_o.grad(f);
        IU::vec3 grad(0,0,0);

        for(size_t k=0;k<_fs.size();k++){
            grad=grad +  _fs[k]->grad(p) * gradOp(k);
        }
        return grad;
    }

};



